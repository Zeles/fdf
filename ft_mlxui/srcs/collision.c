#include "ft_mlxui.h"

int			pointtorectcollision(point *point, rect *rect)
{
	if (rect->start->x < point->x && rect->start->y < point->y)
		if (rect->start->x + rect->width > point->x &&
		rect->start->y + rect->height > point->y)
			return (1);
	return (0);
}

int			recttorectcollision(rect *rect1, rect *rect2)
{
	if (rect1->start->x < rect2->start->x &&
	rect1->start->y < rect2->start->y)
		if (rect1->start->x + rect1->width > rect2->start->x + rect2->width &&
		rect1->start->y + rect1->height > rect2->start->y + rect2->height)
			return (1);
	return (0);
}