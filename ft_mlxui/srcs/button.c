/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   button.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 10:25:12 by gdaniel           #+#    #+#             */
/*   Updated: 2019/01/07 10:56:01 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mlxui.h"

button		*createbutton(rect *cord, rgbcolor *activecolor, rgbcolor *color,
uiconteyner	*conteyner)
{
	button *result;

	result = NULL;
	if (cord != NULL)
	{
		if (!(result = (button*)malloc(sizeof(button))))
			return (result);
		result->cord = cord;
		result->isactive = 0;
		result->activecolor = activecolor;
		result->color = color;
		result->conteyner = conteyner;
	}
	return (result);
}

void		drawbutton(t_window *window, button *button)
{
	point cordxy;
	rgbcolor *color;

	cordxy.y = button->cord->start->y;
	if (button->isactive)
	{
		color = button->activecolor;
		button->isactive = 0;
	}
	else
		color = button->color;
	while (cordxy.y < button->cord->start->y + button->cord->height)
	{
		cordxy.x = button->cord->start->x;
		while (cordxy.x < button->cord->start->x + button->cord->width)
		{
			mlx_pixel_put(window->mlx_ptr, window->window, cordxy.x, cordxy.y,
			convertcolor(color));
			cordxy.x++;
		}
		cordxy.y++;
	}
	drawuiconteyner(window ,button->conteyner);
}

void		clickbutton(button *button, void (*f)())
{
	button->isactive = 1;
	if (f == NULL)
		return ;
	f();
}
