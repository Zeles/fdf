#include "ft_mlxui.h"

label		*createlabel(char *text, point *cord, rgbcolor *color)
{
	label *result;

	result = NULL;
	if(text != NULL && cord != NULL && color != NULL)
	{
		if (!(result = (label*)malloc(sizeof(label))))
			return (result);
		result->str = text;
		result->cord = cord;
		result->color = color;
	}
	return (result);
}

void		drawlabel(t_window *window, label *label)
{
	if (window == NULL || label == NULL)
		return ;
	mlx_string_put(window->mlx_ptr, window->window, label->cord->x,
	label->cord->y, convertcolor(label->color), label->str);
}