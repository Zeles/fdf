#include "ft_mlxui.h"

uiconteyner	*createconteyner(enum uiconteynertype type,
enum anker anker, void *content, rect *rect)
{
	uiconteyner *result;

	result = NULL;
	if(content != NULL && rect != NULL)
	{
		if (!(result = (uiconteyner*)malloc(sizeof(uiconteyner))))
			return (result);
		result->type = type;
		result->anker = anker;
		result->content = content;
		result->rect = rect;
	}
	return (result);
}

void		drawuiconteyner(t_window *window, uiconteyner *conteyner)
{
	if (window == NULL || conteyner == NULL)
		return ;
	if (conteyner->type == LABEL)
		drawlabel(window, (label*)conteyner->content);
}