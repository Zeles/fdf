#include "input.h"

void	update_input(t_window *win)
{
	mlx_mouse_hook(win->window, mouseclick, (void*)0);
	mlx_key_hook(win->window, presskey, (void*)0);
}