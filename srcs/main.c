/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 10:24:13 by gdaniel           #+#    #+#             */
/*   Updated: 2019/01/07 10:24:14 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlxui.h"
#include "input.h"
#include <stdio.h>

void	printbits(int bit)
{
	int i;
	int res;

	i = 0;
	res = 1;
	while (i < sizeof(bit) * 8 - 2)
	{
		res = res << 1;
		i++;
	}
	i = 0;
	while (i < sizeof(bit) * 8 - 2)
	{
		write(1, (bit & res) ? "1" : "0", 1);
		res = res >> 1;
		i++;
	}
	write(1, "\n", 1);
}

int		main(int argc, char **argv)
{
	void	*mlx_ptr;
	rect	*rect;

	mlx_ptr = mlx_init();
	window = createwindow(mlx_ptr, 800, 600, "Test");
	update_input(window);
	rect = createrectptr(createpointptr(100, 25, 0), 25, 100);
	buttons = createbutton(rect, createcolor(255, 0, 0),
	createcolor(0, 255, 0), NULL);
	update(window);
	draw(window);
	mlx_loop(mlx_ptr);
	return (0);
}
