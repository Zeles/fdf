#include "mlxui.h"

imgbutton	*createimgbutton(rect *cord, void *img_ptr, void *activeimg_ptr,
uiconteyner *conteyner)
{
	imgbutton *result;

	result = NULL;
	if (cord != NULL && img_ptr != NULL)
	{
		if (!(result = (imgbutton*)malloc(sizeof(imgbutton))))
			return (result);
		result->cord = cord;
		result->img_ptr = img_ptr;
		result->activeimg_ptr = activeimg_ptr;
		result->isactive = 0;
		result->conteyner = conteyner;
	}
	return (result);
}

void		drawimgbutton(t_window *window, imgbutton *button)
{
	if (button->isactive)
	{
		mlx_put_image_to_window(window->mlx_ptr, window->window,
		button->activeimg_ptr, button->cord->start->x, button->cord->start->y);
		button->isactive = 0;
	}
	else
		mlx_put_image_to_window(window->mlx_ptr, window->window, button->img_ptr,
		button->cord->start->x, button->cord->start->y);
	drawuiconteyner(window, button->conteyner);
}

void		clickimgbutton(imgbutton *button, void (*f)())
{
	button->isactive = 1;
	if (f == NULL)
		return ;
	f();
}
