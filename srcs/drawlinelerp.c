#include "fdf.h"

static void crutchlerpcolor(point *cord, point *delta, int *error, int diry)
{
	cord[0].y = cord[0].y + diry;
	error[0] = error[0] - delta[0].x;
}

void		drawlinelerp(t_window *win, mappoint *start, mappoint *end)
{
	point delta;
	int error;
	point cord;
	int diry;
	int i;

	delta.x = abs(start->cord.x - end->cord.x);
	delta.y = abs(start->cord.y - end->cord.y);
	error = 0;
	delta.z = delta.y;
	cord.y = start->cord.y;
	diry = end->cord.y - start->cord.y;
	diry > 0 ? diry = 1 : 0;
	diry < 0 ? diry = -1 : 0;
	cord.x = start->cord.x;
	i = 0;
	while (i < end->cord.x - start->cord.x)
	{
		mlx_pixel_put(win->mlx_ptr, win->window, cord.x + i, cord.y,
		ft_lerpcolor(&start->color, &end->color, (float)1 / delta.x * i));
		error = error + delta.z;
		2 * error >= delta.x ? crutchlerpcolor(&cord, &delta, &error, diry) : 0;
		i++;
	}
}