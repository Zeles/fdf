#ifndef MLXUI_H
#define MLXUI_H
#include "fdf.h"
#include "../ft_graphics/includes/ft_graphics.h"

enum uiconteynertype
{
	LABEL,
	BUTTON
};

enum anker
{
	UP_LEFT,
	UP_CENTER,
	UP_RIGHT,
	CENTER_LEFT,
	CENTER_CENTER,
	CENTER_RIGHT,
	DOWN_LEFT,
	DOWN_CENTER,
	DOWN_RIGHT
};

typedef	struct	s_uiconteyner
{
	enum uiconteynertype	type;
	enum anker				anker;
	void					*content;
	rect					*rect;
}				uiconteyner;
uiconteyner	*createconteyner(enum uiconteynertype type,
enum anker anker, void *content, rect *rect);
void		drawuiconteyner(t_window *window, uiconteyner *conteyner);

typedef struct	s_label
{
	char *str;
	point *cord;
	rgbcolor *color;
}				label;
label		*createlabel(char *text, point *cord, rgbcolor *color);
void		drawlabel(t_window *window, label *label);

typedef struct	s_button
{
	rect		*cord;
	uiconteyner	*conteyner;
	int			isactive;
	rgbcolor	*activecolor;
	rgbcolor	*color;
}				button;
button		*buttons;
button		*createbutton(rect *cord, rgbcolor *activecolor, rgbcolor *color,
uiconteyner	*conteyner);
void		drawbutton(t_window *window, button *button);
void		clickbutton(button *button, void (*f)());

typedef struct	s_imgbutton
{
	rect		*cord;
	uiconteyner	*conteyner;
	int			isactive;
	void		*img_ptr;
	void		*activeimg_ptr;
}				imgbutton;
imgbutton	*createimgbutton(rect *cord, void *img_ptr, void *activeimg_ptr,
uiconteyner *conteyner);
void		drawimgbutton(t_window *window, imgbutton *button);
void		clickimgbutton(imgbutton *button, void (*f)());

int			pointtorectcollision(point *point, rect *rect);
int			recttorectcollision(rect *start, rect *end);

#endif