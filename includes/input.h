#ifndef INPUT_H
#define INPUT_H
#include "fdf.h"

point	mousepoint;
int		mousekey;
int		keybord;
void	update_input(t_window *win);
int		mouseclick(int button,int x,int y, void *param);
int		presskey(int key, void *param);

#endif