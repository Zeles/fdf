#ifndef FDF_H
#define FDF_H
#include <mlx.h>
#include <stdlib.h>
#include <unistd.h>
#include "../ft_graphics/includes/ft_graphics.h"

t_window	*window;

typedef struct	s_mappoint
{
	point cord;
	rgbcolor color;
}				mappoint;

enum cameratype
{
	ISO,
	CONIC,
	PARALLEL
};

typedef struct	s_camera
{
	point	cord;
	enum cameratype type;
	rgbcolor	*color;
}				camera;

void			update(t_window *window);
void			draw(t_window *window);

void		drawlinelerp(t_window *win, mappoint *start, mappoint *end);

#endif