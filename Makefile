NAME = FDF
SRCS = ./srcs/main.c ./srcs/update.c ./srcs/draw.c ./srcs/inputinit.c ./srcs/mouseclick.c ./srcs/presskey.c
INCLUDES = ./includes/
BUILDFOLDER = ./build/

GRAPHICSFOLDER = ./ft_graphics/
GRAPHICSINCLUDES = $(GRAPHICSFOLDER)includes/
GRAPHICSINK = -I $(GRAPHICSINCLUDES) -L $(GRAPHICSFOLDER) -lftgraphics

MLXUIFOLDER = ./ft_mlxui/
MLXUIINCLUDES = $(MLXUIFOLDER)includes/
MLXUILINK = -I $(MLXUIINCLUDES) -L $(MLXUIFOLDER) -lftmlxui

MLXLINKLINUX = -I /usr/local/include -L /usr/local/lib -lmlx -lXext -lX11
MLXLINTMACOS = -I /usr/local/include -L /usr/local/lib -lmlx -framework OpenGL -framework AppKit

all: $(NAME)

$(NAME): makegraphics makemlxui
	gcc $(SRCS) $(MLXINK) $(GRAPHICSINK) $(MLXUILINK) -I $(INCLUDES) -o $(NAME)

debugmacos: check_folder makegraphics makemlxui
	gcc -g $(SRCS) $(MLXLINTMACOS) $(GRAPHICSINK) $(MLXUILINK) -I $(INCLUDES) -o $(BUILDFOLDER)$(NAME)

debuglinux: check_folder makegraphics makemlxui
	gcc -g $(SRCS) $(MLXLINKLINUX) $(GRAPHICSINK) $(MLXUILINK) -I $(INCLUDES) -o $(BUILDFOLDER)$(NAME)

clean:
	$(MAKE) -C $(GRAPHICSFOLDER) clean
	$(MAKE) -C $(MLXUIFOLDER) clean
	rm -rf *.o

fclean: clean
	$(MAKE) -C $(GRAPHICSFOLDER) fclean
	$(MAKE) -C $(MLXUIFOLDER) fclean
	rm -rf $(NAME)

re:	fclean $(NAME)

re_d_macos: fclean debugmacos

re_d_linux: fclean debuglinux

makegraphics:
	$(MAKE) -C $(GRAPHICSFOLDER)

makemlxui:
	$(MAKE) -C $(MLXUIFOLDER)

check_folder:
	if [ ! -d $(BUILDFOLDER) ]; then \
		mkdir $(BUILDFOLDER); \
	fi