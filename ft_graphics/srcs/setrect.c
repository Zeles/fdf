#include "ft_graphics.h"

rect		setrect(point start, int height, int width)
{
	rect result;

	result.start = &start;
	result.height = height;
	result.width = width;
	return (result);
}

void		setrectptr(rect *rect, point *start, int height, int width)
{
	if (rect != NULL)
	{
		rect->start = start;
		rect->height = height;
		rect->width = width;
	}
}

frect		setfrect(fpoint start, float height, float width)
{
	frect result;

	result.start = &start;
	result.height = height;
	result.width = width;
	return (result);
}

void		setfrectptr(frect *rect, fpoint *start, float height, float width)
{
	if (rect != NULL)
	{
		rect->start = start;
		rect->height = height;
		rect->width = width;
	}
}