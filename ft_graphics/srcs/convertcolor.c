#include "ft_graphics.h"

int			convertcolor(rgbcolor *color)
{
	if (color != NULL)
		return (color->red << 16 | color->green << 8 | color->blue);
	return (0x00FFFFFF);
}