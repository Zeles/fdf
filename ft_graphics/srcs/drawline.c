#include "ft_graphics.h"

void	drawline(t_window *win, point *start, point *end, int color)
{
	point	delta;
	int		error;
	point	cord;
	int		diry;

	delta.x = abs(start->x - end->x);
	delta.y = abs(start->y - end->y);
	error = 0;
	delta.z = delta.y;
	cord.y = start->y;
	diry = end->y - start->y;
	diry > 0 ? diry = 1 : 0;
	diry < 0 ? diry = -1 : 0;
	cord.x = start->x;
	while (cord.x < end->x)
	{
		mlx_pixel_put(win->mlx_ptr, win->window, cord.x, cord.y, color);
		error = error + delta.z;
		if (2 * error >= delta.x)
		{
			cord.y = cord.y + diry;
			error = error - delta.x;
		}
		cord.x++;
	}
}