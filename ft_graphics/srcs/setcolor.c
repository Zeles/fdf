#include "ft_graphics.h"

rgbcolor	setcolor(int red, int green, int blue)
{
	rgbcolor color;

	color.red = red;
	color.green = green;
	color.blue = blue;
	return (color);
}

void		setcolorptr(rgbcolor *color, int red, int green, int blue)
{
	if (color != NULL)
	{
		color->red = red;
		color->green = green;
		color->blue = blue;
	}
}