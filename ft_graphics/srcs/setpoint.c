#include "ft_graphics.h"

point		setpoint(int x, int y, int z)
{
	point result;

	result.x = x;
	result.y = y;
	result.z = z;
	return (result);
}

void		setpointptr(point *point, int x, int y, int z)
{
	if (point != NULL)
	{
		point->x = x;
		point->y = y;
		point->z = z;
	}
}

fpoint		setfpoint(float x, float y, float z)
{
	fpoint result;

	result.x = x;
	result.y = y;
	result.z = z;
	return (result);
}

void		setfpointptr(fpoint *point, float x, float y, float z)
{
	if (point != NULL)
	{
		point->x = x;
		point->y = y;
		point->z = z;
	}
}