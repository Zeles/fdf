#include "ft_graphics.h"

void	minuspoint(point *point, int x, int y, int z)
{
	if(point != NULL)
	{
		point->x -= x;
		point->y -= y;
		point->z -= z;
	}
}

void	minusfpoint(fpoint *point, float x, float y, float z)
{
	if(point != NULL)
	{
		point->x -= x;
		point->y -= y;
		point->z -= z;
	}
}