#include "ft_graphics.h"

void		drawcircle(t_window *win, point *centre, int radius, int color)
{
	point cord;
	int error;

	error = 0;
	cord.x = 0;
	cord.y = radius;
	cord.z = 1 - 2 * radius;
	while (cord.y >= 0)
	{
		mlx_pixel_put(win->mlx_ptr, win->window, centre->x + cord.x, centre->y + cord.y, color);
		mlx_pixel_put(win->mlx_ptr, win->window, centre->x + cord.x, centre->y - cord.y, color);
		mlx_pixel_put(win->mlx_ptr, win->window, centre->x - cord.x, centre->y + cord.y, color);
		mlx_pixel_put(win->mlx_ptr, win->window, centre->x - cord.x, centre->y - cord.y, color);
		error = 2 * (cord.z + cord.y) - 1;
		if (cord.z < 0 && error <= 0)
		{
			cord.z += 2 * ++cord.x + 1;
			continue ;
		}
		if (cord.z > 0 && error > 0)
		{
			cord.z -= 2 * --cord.y + 1;
			continue ;
		}
		cord.z += 2 * (++cord.x - cord.y--);
	}
}