#include "ft_graphics.h"

point *createpointptr(int x, int y, int z)
{
	point *result;

	if (!(result = (point*)malloc(sizeof(point))))
		return (NULL);
	result->x = x;
	result->y = y;
	result->z = z;
	return (result);
}

fpoint	*createfpointptr(float x, float y, float z)
{
	fpoint *result;

	if (!(result = (fpoint*)malloc(sizeof(fpoint))))
		return (NULL);
	result->x = x;
	result->y = y;
	result->z = z;
	return (result);
}