#include "ft_graphics.h"

void		drawrect(t_window *win, rect *rect, int color)
{
	point cord;

	cord.y = rect->start->y;
	while (cord.y <= rect->start->y + rect->height)
	{
		cord.x = rect->start->x;
		while (cord.x <= rect->start->x + rect->width)
		{
			if(cord.x < rect->start->x + rect->width &&
			(cord.y == rect->start->y || cord.y == rect->start->y + rect->height))
				mlx_pixel_put(win->mlx_ptr, win->window, cord.x, cord.y, color);
			if(cord.y < rect->start->y + rect->height &&
			(cord.x == rect->start->x || cord.x == rect->start->x + rect->width))
				mlx_pixel_put(win->mlx_ptr, win->window, cord.x, cord.y, color);
			cord.x++;
		}
		cord.y++;
	}
}