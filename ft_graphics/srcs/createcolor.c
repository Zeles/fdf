#include "ft_graphics.h"

rgbcolor	*createcolor(int red, int green, int blue)
{
	rgbcolor *result;

	if (!(result = (rgbcolor*)malloc(sizeof(rgbcolor))))
		return (NULL);
	result->red = red;
	result->green = green;
	result->blue = blue;
	return (result);
}