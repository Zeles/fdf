#include "ft_graphics.h"

int	ft_lerp(int start, int end, float t)
{
	return (start + t * (end - start));
}

int ft_lerpcolor(rgbcolor *start, rgbcolor *end, float t)
{
	rgbcolor result;

	if (start != NULL && end != NULL)
	{
		result.red = ft_lerp(start->red, end->red, t);
		result.green = ft_lerp(start->green, end->green, t);
		result.blue = ft_lerp(start->blue, end->blue, t);
		return (convertcolor(&result));
	}
	return (0x00FFFFFF);
}