#ifndef FT_GRAPHICS_H
#define FT_GRAPHICS_H
#include <stdlib.h>
#include "mlx.h"

typedef struct	s_window
{
	void		*mlx_ptr;
	void		*window;
	int			size_x;
	int			size_y;
	char		*title;
}				t_window;
t_window	*createwindow(void *mlx_ptr, int size_x, int size_y, char *title);
void		editwindow(t_window **window, int size_x, int size_y, char *title);

typedef struct	s_point
{
	int x;
	int y;
	int z;
}				point;

typedef struct	s_rect
{
	point	*start;
	int		height;
	int		width;
}				rect;
point		*createpointptr(int x, int y, int z);
point		setpoint(int x, int y, int z);
void		setpointptr(point *point, int x, int y, int z);
void		minuspointptr(point *point, int x, int y, int z);
void		plusepointptr(point *point, int x, int y, int z);
rect		*createrectptr(point *start, int height, int width);
rect		setrect(point start, int height, int width);
void		setrectptr(rect *rect, point *start, int height, int width);

typedef struct	s_fpoint
{
	float x;
	float y;
	float z;
}				fpoint;

typedef struct	s_frect
{
	fpoint	*start;
	float	height;
	float	width;
}				frect;
fpoint		*createfpointptr(float x, float y, float z);
fpoint		setfpoint(float x, float y, float z);
void		setfpointptr(fpoint *point, float x, float y, float z);
void		minusfpointptr(fpoint *point, float x, float y, float z);
void		plusefpointptr(fpoint *point, float x, float y, float z);
frect		*createfrectptr(fpoint *start, float height, float width);
frect		setfrect(fpoint start, float height, float width);
void		setfrectptr(frect *rect, fpoint *start, float height, float width);

typedef struct	s_vector
{
	point cord;
	point direction;
}				vector;

typedef struct	s_rgbcolor
{
	short int red;
	short int green;
	short int blue;
}				rgbcolor;
rgbcolor	*createcolor(int red, int green, int blue);
int			convertcolor(rgbcolor *color);
rgbcolor	setcolor(int red, int green, int blue);
void		setcolorptr(rgbcolor *color, int red, int green, int blue);
int			ft_lerpcolor(rgbcolor *start, rgbcolor *end, float t);

void		drawline(t_window *win, point *start, point *end, int color);
void		drawcircle(t_window *win, point *centre, int radius, int color);
void		drawrect(t_window *win, rect *rect, int color);
#endif